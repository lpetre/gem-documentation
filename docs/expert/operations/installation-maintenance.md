# Installation and maintenance

Installation of the GEM online software is supported by the officially
provided `yum` packages. Installation is also possible from source, but
end users are strongly encouraged to use the official packages. More
detailed instructions are provided in the DAQ expert guide

## Installation with `yum`

### Obtain the repo

The GEM online software group maintains a yum repository for the officially released software packages.
To use these on your setup follow the appropriate steps below.

### Determine which packages you require and then install

For most teststands, the following command should be sufficient

``` sh
yum install cmsgemos --enablerepo=cernonly
```

!!! note
    CERN packages and provides some packages that are not available in
    "standard" repositories, e.g., `cx_Oracle`. In order to have `yum`
    find these packages, you should either ensure that the appropriate
    `cernonly` repository file is always enabled, or you should use the
    `--enablerepo=cernonly` flag, as shown in the example.

## Environment setup

### Site specific variables

A number of shell variables should be defined for the software to work
completely. Many of these are set with the environment setup script
mentioned in the previous section, but several must be set for the
specific setup.

| Variable name        | Description                                                                                                                                                                                                          |
|----------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `ELOG_PATH`          | Location the software will place certain plots                                                                                                                                                                       |
| `DATA_PATH`          | Location the software will place output files and will look for intermediate results                                                                                                                                 |
| `GBT_SETTINGS`       | Location the software will look for GBTx configuration settings                                                                                                                                                      |
| `GEM_ONLINE_DB_NAME` | DB SID name from the `tnsnames.ora` file                                                                                                                                                                             |
| `GEM_ONLINE_DB_CONN` | Connection SID and credential, in the following format the `<DB user>/<DB password>` contact the GEM DAQ expert team for the password, but the usual user is `CMS_GEM_APPUSER_R`, the standard GEM DB reader account |

``` bash
export ELOG_PATH=/your/favorite/elog/path
export DATA_PATH=/your/favourite/output/path
export GBT_SETTINGS=/your/favourite/gbt/settings/path
```

### DB connections

Certain tools utilize information stored in the CMS production DB. To
access this functionality from a remote teststand, a tunnel must be
created to the CERN GPN, and an Oracle `tnsnames.ora` file created with
the appropriate connection information.

!!! note
    If the `/etc/tnsnames.ora` file is not present on your system, you
    likely haven\'t installed the `cx_Oracle` dependencies. This package
    will pull in `oracle-instantclient-basic`, which includes a default
    `tnsnames.ora` file.

=== "OMDS (production DB)"

    This will make the CMS OMDS database accessible under the name
    `CMS_OMDS_LOCAL`.
    
    Set up the tunnel manually:
    
    ``` bash
    ssh \
       -L 10151:cmsonr1-v.cern.ch:10121 \
       -L 10152:cmsonr2-v.cern.ch:10121 \
       -L 10153:cmsonr3-v.cern.ch:10121 \
       -L 10154:cmsonr4-v.cern.ch:10121 \
       <username>@lxplus.cern.ch
    ```
    
    Use the tunnel definition in
    `gemos-tunnel-connectivity`{.interpreted-text role="ref"}:
    
    ``` bash
    ssh -Nf cerngpntunnel
    ```
    
    Then add the database descriptors to /etc/tnsnames.ora :
    
    ``` bash
    CMS_OMDS_LOCAL=(
        DESCRIPTION=
        (ADDRESS= (PROTOCOL=TCP) (HOST=localhost) (PORT=10151) )
        (ADDRESS= (PROTOCOL=TCP) (HOST=localhost) (PORT=10152) )
        (ADDRESS= (PROTOCOL=TCP) (HOST=localhost) (PORT=10153) )
        (ADDRESS= (PROTOCOL=TCP) (HOST=localhost) (PORT=10154) )
        (LOAD_BALANCE=on)
        (ENABLE=BROKEN)
        (CONNECT_DATA=
            (SERVER=DEDICATED)
            (SERVICE_NAME=cms_omds_tunnel.cern.ch)
        )
    )
    ```

=== "INT2R (development DB)"

    This will make the CMS INT2R database accessible under the name
    `INT2R_LB_LOCAL`.
    
    Set up the tunnel manually:
    
    ``` bash
    ssh \
       -L 10161:itrac1601-v.cern.ch:10121 \
       -L 10169:itrac1609-v.cern.ch:10121 \
       <username>@lxplus.cern.ch
    ```
    
    Use the tunnel definition in
    `gemos-tunnel-connectivity`{.interpreted-text role="ref"}:
    
    ``` bash
    ssh -Nf cerngpntunnel
    ```
    
    Then add the database descriptors to /etc/tnsnames.ora :
    
    ``` bash
    INT2R_LB_LOCAL = (
        DESCRIPTION =
        (ADDRESS = (PROTOCOL = TCP)(HOST = localhost)(PORT = 10161))
        (ADDRESS = (PROTOCOL = TCP)(HOST = localhost)(PORT = 10169))
        (LOAD_BALANCE = on)
        (ENABLE=BROKEN)
        (CONNECT_DATA =
            (SERVER = DEDICATED)
            (SERVICE_NAME = int2r_lb.cern.ch)
            (FAILOVER_MODE =
                (TYPE = SELECT)
                (METHOD = BASIC)
                (RETRIES = 180)
                (DELAY = 5)
            )
        )
    )
    ```

=== "RCMS DBs"

    This will make the CMS RCMS database accessible under the name
    `CMS_RCMS_LOCAL`.
    
    -   Set up the tunnel manually
    
        ::: important
        ::: title
        Important
        :::
    
        This uses the same tunnels as `CMS_OMDS_LOCAL`, so only need to do
        this once.
        :::
    
        ``` bash
        ssh \
           -L 10151:cmsonr1-v.cern.ch:10121 \
           -L 10152:cmsonr2-v.cern.ch:10121 \
           -L 10153:cmsonr3-v.cern.ch:10121 \
           -L 10154:cmsonr4-v.cern.ch:10121 \
           <username>@lxplus.cern.ch
        ```
    
    -   Use the tunnel definition in
        `gemos-tunnel-connectivity`{.interpreted-text role="ref"}:
    
        ``` bash
        ssh -Nf cerngpntunnel
        ```
    
    Then add the database descriptors to `/etc/tnsnames.ora`:
    
    ``` bash
    CMS_RCMS_LOCAL=(
            DESCRIPTION=
            (ADDRESS= (PROTOCOL=TCP) (HOST=localhost) (PORT=10151) )
            (ADDRESS= (PROTOCOL=TCP) (HOST=localhost) (PORT=10152) )
            (ADDRESS= (PROTOCOL=TCP) (HOST=localhost) (PORT=10153) )
            (ADDRESS= (PROTOCOL=TCP) (HOST=localhost) (PORT=10154) )
            (LOAD_BALANCE=on)
            (ENABLE=BROKEN)
            (CONNECT_DATA=
                    (SERVER=DEDICATED)
                  (SERVICE_NAME=cms_rcms.cern.ch)
            )
    )
    ```

!!! important
    If you use the `~/.ssh/config` tunnel alias method to set up your
    tunnels, you only need to create the tunnels once, and as long as that
    `ssh` process is still running, all the tunnels will be active. If you
    choose the manual route, the same principle applies, but you select
    which subset of ports you open with each `ssh` process.

As mentioned in `gemos-site-variables`{.interpreted-text role="ref"},
two environment variables should be set based on the choice you make
above.

!!! note
    The software using these two varibles concatenates them to
    `GEM_ONLINE_DB_CONN+GEM_ONLINE_DB_NAME`. The main point is that the
    argument that the `cx_Oracle::connect` method requires the connection
    string of the form:

    -   `<DB user>/<DB password>@<DB SID>`

#### Find the Oracle RACs domain names

If the RACs domain names change or you need to access other Oracle CERN
databases, you can use the following procedure:

-   Get the database of interest descriptors in `/etc/tnsnames.ora` on
    `lxplus`
-   Get the IP addresses of all the hosts with a DNS lookup utility
    (e.g. `dig` or `drill`)
-   Perform a reverse DNS lookup of all the retrieved IP addresses (e.g.
    `dig -x` or `dril -x`)
-   Establish the SSH tunnel with the RAC names where the suffix `-s` is
    replaced by `-v`
